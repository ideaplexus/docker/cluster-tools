ARG PROXY_CACHE_PREFIX

FROM ghcr.io/jqlang/jq:1.7.1 AS jq-intermediate
FROM ${PROXY_CACHE_PREFIX}ideaplexus/changelog:v0.11.6 AS changelog-intermediate
FROM ${PROXY_CACHE_PREFIX}hashicorp/vault:1.18.2 AS vault-intermediate
FROM ${PROXY_CACHE_PREFIX}hashicorp/consul-template:0.39.1 AS consul-template-intermediate
FROM ${PROXY_CACHE_PREFIX}alpine/k8s:1.31.3

COPY --from=jq-intermediate /jq /usr/bin/
COPY --from=changelog-intermediate /usr/local/bin/changelog /usr/bin/
COPY --from=vault-intermediate /bin/vault /usr/bin/
COPY --from=consul-template-intermediate /bin/consul-template /usr/bin/

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

ENV WORKDIR=/apps

RUN apk add --no-cache --upgrade \
        ca-certificates=~20240705 \
        findutils=~4.9 \
        grep=~3.11 \
        git=~2.45 \
        gpg=~2.4 \
        make=~4.4 \
        openssh=~9.7 \
        wget=~1.24 \
        python3=~3.12 \
        zip=~3.0 \
        yq-go=~4 \
    && update-ca-certificates \
    && rm -f /usr/lib/python3.12/EXTERNALLY-MANAGED \
    && pip3 install --no-cache-dir --upgrade \
        'ansible-lint>=24.10.0' \
        'ansible>=11.1.0' \
        'httpie>=3.2.4' \
        'Jinja2>=3.1.4' \
        'jmespath>=1.0.1' \
        'pip>=24.3.1' \
        'requests>=2.32.3' \
        'rich>=13.9.4' \
    && rm -rf /var/cache/apk/*

# smoke test \
RUN set -eux \
    ansible --version; \
    ansible-lint --version; \
    aws --version; \
    consul-template --version; \
    changelog; \
    curl --version; \
    git --version; \
    gpg --version; \
    grep --version; \
    helm version; \
    https --version; \
    jq --version; \
    kubectl version --client=true; \
    kustomize version; \
    make --version; \
    ssh -V; \
    vault --version; \
    wget --version; \
    xargs --version; \
    yq --version; \
    zip --version;