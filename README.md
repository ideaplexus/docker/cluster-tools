# Cluster Tools

Tools useful during deployment, based on [alpine/k8s](https://hub.docker.com/r/alpine/k8s).

### Cluster specific tools

* `kubectl`
* `kustomize`
* `helm`
* `helm-diff`
* `helm-unittests`
* `helm-push`
* `aws-iam-authenticator`
* `eksctl`
* `awscli`
* `kubeseal`

### General tools

* `ansible`
* `bash`
* `curl`
* `git`
* `jq`
* `make`
* `ssh`
* `vault`
* `wget`
* `yq`
* `zip`

### How to use
```yaml
some-job:
  image: ideaplexus/toolkit
```